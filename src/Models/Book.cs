using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace program.Models {
    public class Book {
        public int ID { get; set; }

        [Required]
        public string Title { get; set;}
        
        [Display(Name = "Print Date")]
        [DataType(DataType.Date)]
        public DateTime PrintDate { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public decimal Price { get; set; }

        public bool isReviewed { get; set; }
        
    }
}
